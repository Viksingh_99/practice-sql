SHOW DATABASES;
CREATE DATABASE temp;
USE temp;
SHOW TABLES;

-- Creates a relation department in the database:
CREATE TABLE department (
    dept_name VARCHAR(10),
    building VARCHAR(10),
    building_number SMALLINT,
    start_date DATE,
    budget NUMERIC(12 , 2 ) NOT NULL,
    headcount INT,
    code CHAR(3),
    random REAL,
    PRIMARY KEY (dept_name)
);

CREATE TABLE related (
    dept_name VARCHAR(10),
    count INT,
    FOREIGN KEY (dept_name)
        REFERENCES department (dept_name)
);

-- Show table attributes types & constraints
DESC department;
DESC related;

INSERT INTO department VALUES ('wr','ew',2,'2008-11-11',12.01,100,'DEP',12.0789);
INSERT INTO department VALUES ('ws','ew',2,'2008-11-11',11.98,100,'DES',1.098);


-- Delete a table
DROP TABLE departmentl;

-- Add attributes to an existing relation
ALTER TABLE department ADD id INT;
ALTER TABLE department ADD count INT NOT NULL;  

-- Drop attributes from a relation
ALTER TABLE department DROP count;
ALTER TABLE department DROP id;

-- Check table values
SELECT 
    *
FROM
    department;

USE practice;
SHOW TABLES;

-- Find the names of all instructors.
SELECT 
    name
FROM
    instructor;

-- Find the department names of all instructors,
SELECT 
    dept_name
FROM
    instructor;

-- Find the department names of all instructors, remove duplicate 
SELECT DISTINCT
    dept_name
FROM
    instructor;

-- Increase instructor salary by 10%
SELECT 
    name, salary, salary * 1.1 AS incremented
FROM
    instructor;

-- Find the names of all instructors in the Computer Science department who have salary greater than $70,000.
SELECT 
    name, salary
FROM
    instructor
WHERE
    salary > 70000
        AND dept_name = 'Comp. Sci.';
        
-- Retrieve the names of all instructors, along with their department names and department building name.
SELECT 
    name, i.dept_name, building
FROM
    instructor AS i,
    department AS d
WHERE
    i.dept_name = d.dept_name;

-- For all instructors in the university who have taught some course, ﬁnd their names and the course ID of all courses they taught.
SELECT 
    i.name, t.course_id
FROM
    instructor AS i,
    teaches AS t
WHERE
    i.ID = t.ID;
    
    
-- Instructor names and course identiﬁers for instructors in the Computer Science department.
SELECT 
    name, course_id, dept_name
FROM
    instructor,
    teaches
WHERE
    instructor.ID = teaches.ID
        AND dept_name = 'Comp. Sci.';

-- Find the names of all instructors whose salary is greater than at least one instructor in the Biology department.
SELECT DISTINCT
    i1.name, i1.salary, i1.dept_name
FROM
    instructor AS i1,
    instructor AS i2
WHERE
    i1.salary > i2.salary
        AND i2.dept_name = 'Biology';

-- Find the names of all departments whose building name includes the substring 'Watson'.
SELECT 
    dept_name
FROM
    department
WHERE
    building LIKE '%Watson%';
    
-- All attributes of instructor that teach something
SELECT 
    i.*
FROM
    instructor AS i,
    teaches AS t
WHERE
    i.ID = t.ID;

-- List in alphabetic order all instructors in the Physics department
SELECT 
    *
FROM
    instructor
WHERE
    dept_name = 'Physics'
ORDER BY name;

-- List the entire instructor relation in descending order of salary. If several instructors have the same salary, we order them in ascending order by name.
SELECT 
    *
FROM
    instructor
ORDER BY salary DESC , name ASC;

-- Find the names of instructors with salary amounts between $90,000 and $100,000,
SELECT 
    name
FROM
    instructor
WHERE
    salary BETWEEN 90000 AND 100000;
    
-- Convert first query to use tuple constructor
SELECT 
    name, course_id
FROM
    instructor,
    teaches
WHERE
    instructor.ID = teaches.ID
        AND dept_name = 'Biology';

SELECT 
    name, course_id
FROM
    instructor AS i,
    teaches AS t
WHERE
    (i.ID , dept_name) = (t.ID , 'Biology');

-- ﬁnd all instructors who appear in the instructor relation with null values for salary
SELECT 
    *
FROM
    instructor
WHERE
    salary IS NULL;

SELECT 
    *
FROM
    instructor
WHERE
    salary > 100 IS UNKNOWN;

-- Find the average salary of instructors in the Computer Science department.
SELECT 
    AVG(salary) AS avg_salary
FROM
    instructor
WHERE
    dept_name = 'Comp. Sci.';

-- Find the total number of instructors who teach a course in the Spring 2018 semester.
SELECT 
    COUNT(DISTINCT ID) AS count
FROM
    teaches
WHERE
    semester = 'Spring' AND year = 2018;
    
-- Find number of tuples in course relation.    
SELECT 
    COUNT(*)
FROM
    course;

-- Find the average salary in each department.
SELECT 
    dept_name, AVG(salary) as average_salary
FROM
    instructor
GROUP BY (dept_name);

-- Find the average salary of all instructors.
SELECT 
    AVG(salary)
FROM
    instructor;

-- Find the number of instructors in each department who teach a course in the Spring 2018 semester.
select dept_name, count(distinct instructor.ID) from instructor,teaches where (instructor.id,teaches.semester,year) = (teaches.id,'Spring',2018) group by dept_name; 



SELECT 
    dept_name, COUNT(DISTINCT instructor.ID) as instructor_count
FROM
    instructor,
    teaches
WHERE
    (instructor.ID , teaches.semester, year) = (teaches.ID , 'Spring', 2018)
GROUP BY dept_name;

-- only those departments where the average salary of the instructors is more than $42,000.
SELECT 
    dept_name, AVG(salary) as average_salary
FROM
    instructor
GROUP BY dept_name
HAVING  average_salary > 42000;

-- For each course section oﬀered in 2017, ﬁnd the average total credits (tot cred) of all students enrolled in the section, if the section has at least 2 students.
SELECT 
    course_id, semester, sec_id, SUM(tot_cred)
FROM
    student,
    takes
WHERE
    year = 2017
GROUP BY course_id , semester , year , sec_id
HAVING COUNT(takes.ID) >= 2;

-- Find all the courses taught in the both the Fall 2017 and Spring 2018 semesters.
-- We use distinct here because the intersection by default removes duplicate
SELECT 
    DISTINCT course_id
FROM
    section
WHERE
    (semester , year) = ('Fall' , 2017)
        AND course_id IN (SELECT DISTINCT
            course_id
        FROM
            section
        WHERE
            (semester , year) = ('Spring' , 2018));
            
            
-- Find all the courses taught in the Fall 2017 semester but not in the Spring 2018 semester,
SELECT 
    *
FROM
    section
WHERE
    (semester , year) = ('Fall' , 2017)
        AND course_id NOT IN (SELECT DISTINCT
            course_id
        FROM
            section
        WHERE
            (semester , year) = ('Spring' , 2018));

-- The names of instructors whose names are neither “Mozart” nor “Einstein”.
SELECT 
    *
FROM
    instructor
WHERE
    name NOT IN ('Mozart' , 'Einstein');
    
-- Find the total number of (distinct) students who have taken course sections taught by the instructor with ID 110011
SELECT 
    *
FROM
    instructor
WHERE
    salary > (SELECT 
            MIN(salary)
        FROM
            instructor
        WHERE
            dept_name = 'Biology');

SELECT 
    *
FROM
    instructor
WHERE
    salary > SOME (SELECT 
            salary
        FROM
            instructor
        WHERE
            dept_name = 'Biology');
            
-- Let us ﬁnd the names of all instructors that have a salary value greater than that of each instructor in the Biology department.
SELECT 
    *
FROM
    instructor
WHERE
    salary > ALL (SELECT 
            salary
        FROM
            instructor
        WHERE
            dept_name = 'Biology');

-- Find the departments that have the highest average salary.
SELECT 
    dept_name
FROM
    instructor
GROUP BY dept_name
HAVING AVG(salary) >= ALL (SELECT 
        AVG(salary)
    FROM
        instructor
    GROUP BY dept_name);

-- Find all courses taught in both the Fall 2017 semester and in the Spring 2018 semester
SELECT 
    course_id
FROM
    section AS S
WHERE
    semester = 'Fall' AND year = 2017
        AND EXISTS( SELECT 
            *
        FROM
            section AS T
        WHERE
            semester = 'Spring' AND year = 2018
                AND S.course_id = T.course_id);
                
-- Find all students who have taken all courses oﬀered in the Biology department.

-- Find the total number of (distinct) students who have taken course sections taught by the instructor with ID 110011.

-- Find those departments with the maximum budget

-- Find all departments where the total salary is greater than the average of the total salary at all departments.

-- Lists all departments along with the number of instructors in each department
SELECT 
    dept_name,
    (SELECT 
            COUNT(DISTINCT ID)
        FROM
            instructor
        WHERE
            instructor.dept_name = department.dept_name)
FROM
    department;

-- To ﬁnd the average number of sections taught (regardless of year or semester) per instructor, with sections taught by multiple instructors counted once per instructor.
SELECT 
    (SELECT 
            COUNT(*)
        FROM
            teaches) / (SELECT 
            COUNT(*)
        FROM
            instructor)
FROM DUAL;


