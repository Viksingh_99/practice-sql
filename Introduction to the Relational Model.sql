show databases;
use practice;
show tables;

desc instructor;
select * from instructor;

-- to ﬁnd the information about all the instructors who work in the Watson building.
select * from department where building like 'Watson';

desc section;
select * from section;

