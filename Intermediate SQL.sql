SHOW DATABASES;
USE practice;
SHOW TABLES;

-- For each student the set of courses a student has taken
SELECT 
    name, course_id
FROM
    student
        NATURAL JOIN
    takes
ORDER BY name;

-- List the names of students along with the titles of courses that they have taken.
SELECT 
    name, title
FROM
    (student
    NATURAL JOIN takes)
        JOIN
    course USING (course_id)
ORDER BY name;

-- For each student the set of courses a student has taken (using on)
SELECT 
    name, course_id
FROM
    student
        JOIN
    takes ON student.id = takes.id;

-- A list of all students, displaying their ID, and name, dept name, and tot cred, along with the courses that they have taken include students who have not taken any course.
SELECT 
    *
FROM
    student
        NATURAL LEFT OUTER JOIN
    takes;

-- Find all students who have not taken a course
SELECT 
    *
FROM
    student
        NATURAL LEFT OUTER JOIN
    takes
WHERE
    takes.id IS NULL;
    
-- Display a list of all students in the Comp. Sci. department, along with the course sections, if any, that they have taken in Spring 2017;  all course sections from Spring 2017 must be displayed, even if no student from the Comp. Sci. department has taken the course section.
SELECT 
    *
FROM
    student
        LEFT OUTER JOIN
    takes ON student.id = takes.id;

-- the two give diffrent results 
SELECT 
    *
FROM
    student
        LEFT OUTER JOIN
    takes ON TRUE
WHERE
    student.id = takes.id;
    
-- List of all course sections oﬀered by the Physics department in the Fall 2017 semester, with the building and room number of each section
SELECT 
    *
FROM
    course
        NATURAL JOIN
    section
WHERE
    (dept_name , year, semester) = ('Physics' , 2017, 'Fall')
ORDER BY course_id;

-- Creating a view
CREATE VIEW faculty AS
    (SELECT 
        ID, name, dept_name
    FROM
        instructor);

-- A view onlist of all course sections oﬀered by the Physics department in the Fall 2017 semester, with the building and room number of each section.
CREATE VIEW physics_fall_2017 AS
    SELECT 
        *
    FROM
        course
            JOIN
        section USING (course_id)
    WHERE
        (dept_name , semester, year) = ('Physics' , 'Fall', 2017);
        
-- Using a view name as the relation name
SELECT 
    *
FROM
    physics_fall_2017
WHERE
    building = 'Watson';
        
-- Using attribute name in view
CREATE VIEW departments_total_salary (dept_name , total_salary) AS
    SELECT 
        dept_name, SUM(salary)
    FROM
        instructor
    GROUP BY dept_name;

SELECT 
    *
FROM
    departments_total_salary;

-- A view physics_fall_2017_watson that lists the course ID and room number of all Physics courses oﬀered in the Fall 2017 semester in the Watson building
CREATE VIEW physics_fall_2017_watson AS
    SELECT 
        course_id, room_number
    FROM
        physics_fall_2017
    WHERE
        building = 'Watson';

CREATE VIEW history_instructors AS
    (SELECT 
        *
    FROM
        instructor
    WHERE
        dept_name = 'History');

INSERT INTO history_instructors VALUES ('25566', 'Brown', 'Biology', 100000);

CREATE INDEX test ON student (dept_name);

CREATE UNIQUE INDEX dept_index ON instructor (dept_name);


